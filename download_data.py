"""Download COVID-19 data."""

import pandas as pd
import requests

import argparse
from collections import defaultdict
from configparser import ConfigParser
from datetime import datetime
from functools import lru_cache
import logging
import os
import re
import time

logging.basicConfig(level=logging.INFO)


X_RATELIMIT_REMAINING_HEADER = 'X-RateLimit-Remaining'


class Downloader:
    """Download COVID-19 data."""

    def __init__(self, config_file):
        self.config_file = config_file
        self.config = ConfigParser()
        with open(config_file) as f:
            self.config.read_file(f)
        self.reports_url = self.config['urls']['reports']
        self.throttle_threshold = 1
        self.max_retry_delay = 16
        if self.config.has_section('options'):
            if 'throttle_threshold' in self.config['options'].keys():
                self.throttle_threshold = int(self.config['options']['throttle_threshold'])
            if 'max_retry_delay' in self.config['options'].keys():
                self.max_retry_delay = int(self.config['options']['max_retry_delay'])
        self.ratelimit_remaining = self.throttle_threshold + 1


    def throttle(self):
        """Wait until remaining rate limit resource increases
        above threshold."""
        t = 1
        backoff = 2
        while self.ratelimit_remaining < self.throttle_threshold:
            logging.warning(f"low ratelimit_remaining: {self.ratelimit_remaining}")
            time.sleep(t)
            t *= backoff
            if t > self.max_retry_delay:
                raise RetryError("too many retries")
            res = requests.get(self.reports_url)
            self.ratelimit_remaining = int(res.headers[X_RATELIMIT_REMAINING_HEADER])


    def prepare(self, date, iso):
       """Prepare a request from date and iso parameters.

       Parameters
       ----------
       date : str
           YYYY-MM-DD specifying the data vintage;
           reporting date, not download date
       iso : str
           three-character country code

       Returns
       -------
       url : str
           url with get parameters
       """
       req = requests.Request()
       req.url = self.reports_url
       req.params = {'date': date, 'iso': iso, 'per_page': 20}
       return req.prepare().url


    def check_data(self, data):
        """Check well-formedness of data.

        Data must have the following shape:

        {'date', 'region': {'iso'}, 'num_confirmed',
         'num_deaths', 'num_recovered'}
        """
        assert 'region' in data, 'region not in data'
        assert 'iso' in data['region'], 'iso not in data'
        assert 'confirmed' in data, 'confirmed not in data'
        assert 'deaths' in data, 'deaths not in data'
        assert 'recovered' in data, 'recovered not in data'


    def collate(self, data):
        """Combine data under the same date, ISO.

        Aggregate over all regions in the response.
        """
        collated = defaultdict(dict)
        for d in data:
           self.check_data(d)
           date = d['date']
           iso = d['region']['iso']
           k = (date, iso)
           focus = collated[k]
           focus['date'] = date
           focus['iso'] = iso
           for src in ('confirmed', 'deaths', 'recovered'):
               tgt = f'num_{src}'
               focus[tgt] = focus.get(tgt, 0) + d[src]
        return collated


    @lru_cache
    def download(self, url):
       """Download data for date-iso combination.

       Parameters
       ----------
       date : str
           YYYY-MM-DD specifying the data vintage;
           reporting date, not download date
       iso : str
           three-character country code

       Returns
       -------
       data : object
           value associated with data key from JSON response
       next_page_url : str
           url of next page of data
       """
       self.throttle()
       res = requests.get(url)
       if res.status_code != 200:
           raise HTTPReturnCodeException(res.status_code)
       self.ratelimit_remaining = int(res.headers[X_RATELIMIT_REMAINING_HEADER])
       content = res.json()
       return content.get('data'), content.get('next_page_url')


    def download_data(self, params):
        """Download COVID-19 data.

        Parameters
        ----------
        params : list of tuple of str, str
            each two-tuple specifies a date-country
            combination to query

        Returns
        -------
        df : pandas.DataFrame
            downloaded data; has columns:

            date          - data vintage (not download date)
            iso           - three-digit country code
            num_confirmed - number of confirmed cases (cumulative)
            num_deaths    - number of deaths (cumulative)
            num_recovered - number of recovered cases (cumulative)
        """
        downloaded = defaultdict(dict)
        for date, iso in params:
            url = self.prepare(date, iso)
            while url:
                data, url = self.download(url)
                data = self.collate(data)
                if not data:
                    logging.info(f"no data for available {date}, {iso}")
                    continue
                for k in data:
                    focus = downloaded[k]
                    focus['date'] = date
                    focus['iso'] = iso
                    for col in ('num_confirmed', 'num_deaths', 'num_recovered'):
                        focus[col] = focus.get(col, 0) + data[k][col]
        # convert from {(date, iso) = {'date', 'iso', ... }} to 
        #         to   {'date': values, 'iso': values, ... }
        reshaped = defaultdict(list)
        for entry in sorted(downloaded.values(), key=lambda entry: (entry['date'], entry['iso'])):
            for col in ('date', 'iso', 'num_confirmed', 'num_deaths', 'num_recovered'):
                reshaped[col].append(entry[col])
        return pd.DataFrame(reshaped)


class FileIsNotDirError(Exception):
    pass


class HTTPReturnCodeError(Exception):
    pass


class RetryError(Exception):
    pass


def parse_dates(dates):
    """Parse dates and check for well-formedness."""
    parsed = []
    for date in dates:
        if type(date) is pd.Timestamp:
            parsed.append(date.strftime('%Y-%m-%d'))
        elif re.match(r'\d{4}-\d\d-\d\d', str(date)):
            parsed.append(str(date))
        else:
            logging.warning(f"skipping invalid date: {date}")
    return parsed


def parse_isos(isos):
    """Parse ISO codes and check for well-formedness."""
    parsed = []
    for iso in isos:
        if len(str(iso)) == 3:
            parsed.append(str(iso))
        else:
            logging.warning(f"skipping invalid iso: {iso}")
    return parsed


def load_params(params_file, generate_combinations):
    """Load date, ISO combinations from file.

    Parameters
    ----------
    params_file : str
        name of the Excel file containing dates and ISO codes
    generate_combinations : bool
        treat each column as a list of unique values from
        which every combination of values is to be generated

    Returns
    -------
    params : list of tuples of str, str
        each two-tuple in params corresponds to one combination
        of YYYY-MM-DD date and three-character ISO code
    """
    df = pd.read_excel(params_file)
    dates = parse_dates(df.iloc[:, 0])
    isos = parse_isos(df.iloc[:, 1])

    if not dates:
        raise ValueError(f"no valid dates in file: {params_file}")
    if not isos:
        raise ValueError(f"no valid ISO codes in file: {params_file}")

    params = list(zip(dates, isos))
    if not generate_combinations and len(dates) != len(isos):
        raise ValueError(f"number of valid dates does not match number of valid ISO codes in {params_file}")
    elif generate_combinations:
        params = []
        for date in sorted(set(dates)):
            for iso in sorted(set(isos)):
                params.append((date, iso))
    return params


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Download COVID-19 data.")
    parser.add_argument("--generate_combinations", action="store_true",
        help="generate date-country combinations from configuration file")
    parser.add_argument("--noclobber", action="store_true",
        help="do not overwrite output path")
    parser.add_argument("--params_file", default="params.xlsx",
        help="name of the file containing date/ISO parameter values")
    parser.add_argument("--config_file", default="config.ini",
        help="name of the file containing urls, runtime options, etc.")
    parser.add_argument("--output_path", help="file to which to save data")
    return parser.parse_args()


def default_path():
    """Generate the default path to which to save the output.

    This function will be called if output_path is not specified
    at command invocation.
    """
    default_dir = "data"
    if not os.path.exists(default_dir):
        os.mkdir(default_dir)
    if not os.path.isdir(default_dir):
        raise FileIsNotDirError(default_dir)
    ts = datetime.now().strftime('%Y%m%d%H%M%S')
    return os.path.join(default_dir, f"{ts}.xlsx")


def clobber_check(noclobber, path):
    """Verify that noclobber is False or path
    does not exist.

    Parameters
    ----------
    noclobber : bool
        if True, raise FileExistsError if
        path exists

    Returns
    -------
    passes : bool
        check passes if noclobber is False
        or path does not exist
    """
    if noclobber and os.path.exists(path):
        raise FileExistsError(path)
    return True


def save(df, path, noclobber=False):
    """Save data to Excel file named by path.

    Parameters
    ----------
    df : pandas.DataFrame
        downloaded data; has columns:

        date          - data vintage (not download date)
        iso           - three-digit country code
        num_confirmed - number of confirmed cases (cumulative)
        num_deaths    - number of deaths (cumulative)
        num_recovered - number of recovered cases (cumulative)
    path : str
        name of Excel file which to save data
    noclobber : bool
        preserve existing file, if it exists?

    Returns
    -------
    n : int
        number of output records
    """
    clobber_check(noclobber, path)
    df.to_excel(path)
    return len(df)


def main():
    """Parse command line arguments and download data."""
    args = parse_args()
    output_path = args.output_path if args.output_path else default_path()
    # fail before download if noclobber and output path exists
    clobber_check(args.noclobber, output_path)
    params = load_params(args.params_file, args.generate_combinations)
    downloader = Downloader(args.config_file)
    data = downloader.download_data(params)
    n = save(data, output_path, args.noclobber)
    logging.info(f"wrote {n} records to {output_path}")


if __name__ == '__main__':
    main()
