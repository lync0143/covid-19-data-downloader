"""Download available ISO codes."""

import os
import pandas as pd
import requests

import argparse
from configparser import ConfigParser
import logging

logging.basicConfig(level=logging.INFO)


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Download available ISO codes.")
    parser.add_argument("--config_file", default="config.ini",
        help="name of ini-style config file")
    parser.add_argument("--iso_codes_file", default="iso_codes.xlsx",
        help="name of the Excel spreadsheet file to which to save the list of ISOs")
    return parser.parse_args()


def load_config(config_file):
    """Load ini-style config."""
    config = ConfigParser()
    with open(config_file) as f:
        config.read_file(f)
    return config


def get(url, params=None):
    """Get the JSON at url."""
    res = requests.get(url, params)
    assert res.status_code == 200, f"failed to get url: {url}"
    return res.json()


def download_iso_codes(iso_codes_url):
    """Download ISO codes."""
    content = get(iso_codes_url, params={'per_page': 20, 'page': 1})
    isos = content['data']
    next_page_url = content['next_page_url']
    while next_page_url:
        content = get(next_page_url)
        isos.extend(content['data'])
        next_page_url = content['next_page_url']
    return list(sorted([entry['iso'] for entry in isos if len(entry['iso']) == 3]))


def save(isos, filename):
    """Save ISOs to Excel spreadsheet named by filename."""
    df = pd.DataFrame({'ISO': isos})
    mode = 'a' if os.path.exists(filename) else 'w'
    with pd.ExcelWriter(filename, mode=mode) as writer:
        df.to_excel(writer, sheet_name="ISO Codes")


def main():
    """Parse command line arguments and
    download available ISO codes."""
    args = parse_args()
    config = load_config(args.config_file)
    isos = download_iso_codes(config["urls"]["iso_codes"])
    save(isos, args.iso_codes_file)
    logging.info(f"Wrote ISO codes: {len(isos)}")
    logging.info(f"Wrote to file: {args.iso_codes_file}")



if __name__ == '__main__':
    main()
