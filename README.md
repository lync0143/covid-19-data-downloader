Download COVID-19 Statistics
============================

Download number of confirmed, dead, and recovered
COVID-19 patients by three-character ISO  country
code and YYYY-MM-DD date.

<b>Note: this software requires Python 3.8 or above.</b>

Quickstart
----------

```bash
git clone git@gitlab.com:lync0143/covid-19-data-downloader.git
cd covid-19-data-downloader
python -m venv covid19
source covid19/bin/activate
pip install -r requirements.txt
python download_data.py
```

Instructions
------------

1. Install dependencies:
       pip install -r requirements.txt

2. Specify the dates and countries for which to
   download data in params.xlsx. By default,
   params.xlsx is assumed to be "tidy," that is,
   each row in the first sheet of the file
   specifies a date-country combination for which
   data is to be downloaded.  If instead you
   want the downloader to infer combinations from
   lists of unique dates and countries, specify
   the --generate_combinations flag when invoking
   the downloader.

3. Invoke the downloader.  The downloader reads
   params.xlsx to determine which dates and
   countries for which it is to download data.
   By default, the downloader reads a single
   row of date-country code parameter values
   from the file at a time, and downloads and
   collates the requested data, if any. Specify
   the --generate_combinations flag to treat
   the date and iso columns as lists of unique
   values, from which the downloaded will
   generate all possible combinations of date-
   country parameter value pairs.

4. View the downloaded data at
   data/YYYYmmddHHMMSS.xlsx, where YYYYmmddHHMMSS
   is the timestamp of the file's creation.

Usage
-----
<p>
<details>
<summary>
    usage: download_data.py [-h] [--generate_combinations] [--noclobber] [--params_file PARAMS_FILE]
                            [--config_file CONFIG_FILE] [--output_path OUTPUT_PATH]
</summary>
  Download COVID-19 data.

  optional arguments:
    -h, --help            show this help message and exit
    --generate_combinations
                          generate date-country combinations from configuration file
    --noclobber           do not overwrite output path
    --params_file PARAMS_FILE
                          name of the file containing date/ISO parameter values
    --config_file CONFIG_FILE
                          name of the file containing urls, runtime options, etc.
    --output_path OUTPUT_PATH
                          file to which to save data
</details>
</p>

Examples
--------

    # default, "tidy" invocation
    python download_data.py

    # generate combinations of date-country values
    python download_data.py --generate_combinations

Schema
------

The data will have the following columns:

| Column        | Description                                               |
| ------------- | --------------------------------------------------------- |
| date          | Data is described as of this date                         |
| iso           | Three-character country code                              |
| num_confirmed | Cumulative confirmed cases, as of date                    | 
| num_deaths    | Cumulative deaths, as of date                             |
| num_recovered | Cumulative recovered, as of date                          |

Supporting files
----------------

### params.xlsx

Spreadsheet with two columns, one for dates and one for
ISO country codes.  By default, the downloader will
download data for each date/country code combination in
params.xlsx.  Alternatively, `download_data.py` can be
invoked with `--generate_combinations` to treat each
column as an independent list of unique values to
use to generate date/code combinations.

### config.ini

Additional configuration parameters for the downloader.

### download_iso_codes.py

Download ISO codes from the API.

### tests/test*.py

Test the downloader.

Install pytest to run tests:

    pip install pytest

Then run `PYTHONPATH=. pytest`.

### README.md

This file.

### requirements.txt

Use with pip to install dependencies:

    pip install -r requirements.txt
