"""Test parse_dates in download_data.py."""

from download_data import parse_dates

def test_parse_one_date():
    """A single YYYY-MM-DD date."""
    date = ['2021-05-13']
    assert parse_dates(date) == date

def test_parse_multiple_dates():
    """Multiple YYYY-MM-DD dates."""
    dates = ['2020-01-01', '1999-06-03', '2021-05-13']
    assert parse_dates(dates) == dates

def test_ignore_invalid_date():
    """Ignore non-YYYY-MM-DD date."""
    all_dates = ['1900-03-20', '2000-09-05', '08-10-2025']
    valid_dates = all_dates[:-1]
    assert parse_dates(all_dates) == valid_dates
