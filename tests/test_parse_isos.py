"""Test parse_isos in download_data.py."""

from download_data import parse_isos

def test_parse_one_iso():
    """A single three-character ISO code."""
    isos = ['GBR', 'MEX', 'USA']
    assert parse_isos(isos) == isos

def test_parse_multiple_isos():
    """Multiple three-character ISO codes."""
    iso = ['MEX']
    assert parse_isos(iso) == iso

def test_ignore_invalid_iso():
    """Ignore invalid ISO code."""
    all_isos = ['INVALID', 'MEX', 'USA']
    valid_isos = all_isos[1:]
    assert parse_isos(all_isos) == valid_isos
