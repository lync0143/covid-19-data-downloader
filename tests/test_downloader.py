from download_data import Downloader

config_file = 'config.ini'
downloader = Downloader(config_file)

def test_download_data():
    """Download data."""
    params = [
        ('2020-06-20', 'GBR'),
        ('2020-06-20', 'USA'),
        ('2020-10-17', 'BRA')]
    assert len(downloader.download_data(params)) == len(params)


def test_attempt_to_download_too_early():
    """Attempt to download data for too-early date."""
    params = [('2000-10-01', 'USA')]
    assert len(downloader.download_data(params)) == 0


def test_attempt_to_download_nonexistent_iso():
    """Attempt to download data for nonexistent ISO code."""
    params = [('2020-06-20', 'ZZZ')]
    assert len(downloader.download_data(params)) == 0
