"""Test saving downloaded data to a file."""

from download_data import Downloader
from download_data import save

import os


def test_save_data(tmp_path):
    data_path = tmp_path / "data.xlsx"
    params = [('2020-07-15', 'USA')]
    downloader = Downloader('config.ini')
    df = downloader.download_data(params)
    save(df, data_path)
    assert os.path.exists(data_path)
